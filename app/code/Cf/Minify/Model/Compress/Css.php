<?php
/**
 * Codefathers Minify for JS and CSS
 *
 * @category    Cf
 * @package     Cf_Minify
 * @copyright   Copyright (c) codefathers 2017
 */



/**
 * Class Cf_Minify_Model_Compress_Css
 */
class Cf_Minify_Model_Compress_Css extends Cf_Minify_Model_Compress_Abstract
{


    /**
     * abtract function to do the compression
     */
    protected function _minify($text)
    {
        return $text;
    }

}

