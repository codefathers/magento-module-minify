<?php
/**
 * Codefathers Minify for JS and CSS
 *
 * @category    Cf
 * @package     Cf_Minify
 * @copyright   Copyright (c) codefathers 2017
 */


/**
 * Class Cf_Minify_Model_Compress_Comment
 */
class Cf_Minify_Model_Compress_Comment extends Cf_Minify_Model_Compress_Abstract
{


    /**
     * abtract function to do the compression
     */
    protected function _minify($text)
    {
        /* delete one-line comments */
        //$text = preg_replace("/^(\/\/.*)/", "/*COMMENT1*/\n", $text);

        /* delete multi-line comments */
        //$text = preg_replace("/(\/\*(.{10,})\*\/)/sU", "/*COMMENT2*/\n", $text);


        /* replace line breaks */
        $text = str_replace("\r\n\r\n", "\n", $text);
        $text = str_replace("\n\n", "\n", $text);

        /* replace tabs */
        $text = str_replace("\t", "", $text);

        return $text;
    }



}

