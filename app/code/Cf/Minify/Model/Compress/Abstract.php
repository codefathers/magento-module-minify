<?php
/**
 * Codefathers Minify for JS and CSS
 *
 * @category    Cf
 * @package     Cf_Minify
 * @copyright   Copyright (c) codefathers 2017
 */


/**
 * Class Cf_Minify_Model_Compress_Abstract
 */
abstract class Cf_Minify_Model_Compress_Abstract extends Varien_Object
{

    /**
     * @param $type
     */
    public static function factory($type, Mage_Core_Model_Store $store = null)
    {
        $store = ($store) ? $store : Mage::app()->getStore();
        $result = Mage::getSingleton("cf_minify/compress_{$type}");
        $result->setStore($store);
        return $result;
    }


    /**
     *
     * does the compression
     *
     * @param string $text
     * @return string
     */
    final public function minify($text)
    {
        return $this->_doMinify($text);
    }

    /**
     * abtract function to do the compression
     * @param string $text
     * @return string
     */
    abstract protected function _minify($text);


}

