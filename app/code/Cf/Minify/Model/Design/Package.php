<?php
/**
 * Codefathers Minify for JS and CSS
 *
 * @category    Cf
 * @package     Cf_Minify
 * @copyright   Copyright (c) codefathers 2017
 */



class Cf_Minify_Model_Design_Package extends Mage_Core_Model_Design_Package
{

    /**
     * Merge specified javascript files and return URL to the merged file on success
     *
     * @param $files
     * @return string
     */
    public function getMergedJsUrl($files)
    {
        $targetDir = $this->_initMergerDir('js');
        if (!$targetDir) {
            throw new Exception ("JavaScript merge dir is not writable (normally media/js).");
        }
        $doCompress = Mage::getStoreConfigFlag('dev/js/compress_code');
        if (!$doCompress) {
            return $this->_originalGetMergedJsUrl($files);
        }

        // Compress JS
        $targetFilename = md5(implode(',', $files).',compress') . '.js';
        $beforeMergeCallback = array($this, 'beforeMergeJs');
        if (Mage::helper('core')->mergeFiles($files, $targetDir . DS . $targetFilename, false, $beforeMergeCallback, 'js')) {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'js/' . $targetFilename;
        }
        return '';
    }

    /**
     * Merge specified javascript files and return URL to the merged file on success
     *
     * @param $files
     * @return string
     */
    private function _originalGetMergedJsUrl($files)
    {
        $targetFilename = md5(implode(',', $files)) . '.js';
        $targetDir = $this->_initMergerDir('js');
        if (!$targetDir) {
            return '';
        }
        if (Mage::helper('core')->mergeFiles($files, $targetDir . DS . $targetFilename, false, null, 'js')) {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'dmedia/js/' . $targetFilename;
        }
        return '';
    }

    /**
     * Make sure merger dir exists and writeable
     * Also can clean it up
     *
     * @param string $dirRelativeName
     * @param bool $cleanup
     */
    protected function _initMergerDir($dirRelativeName, $cleanup = false)
    {
        return parent::_initMergerDir($dirRelativeName, $cleanup);
        try {
            if ($dirRelativeName == 'js') {
                $dir = Mage::getBaseDir('media') . DS . 'js' . DS . $dirRelativeName;
            } else {
                $dir = Mage::getBaseDir('media') . DS . $dirRelativeName;
            }
            if ($cleanup) {
                Varien_Io_File::rmdirRecursive($dir);
            }
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            return is_writeable($dir) ? $dir : false;
        } catch (Exception $e) {
            Mage::helper('vorwerk_minify')->log($e->getMessage(), Zend_Log::CRIT);
        }
        return false;
    }


    /**
     * Before merge js callback function
     *
     * @param string $file
     * @param string $contents
     * @return string
     */
    public function beforeMergeJs($file, $contents)
    {
        $doCompress = Mage::getStoreConfigFlag('dev/js/compress_code');
        if ($doCompress) {
            $doMinify = $this->_isMinifyable($file,$contents);
            if ($doMinify) {
                try {
                    $min = Mage::getSingleton('vorwerk_minify/compress_js')->minify($contents);
                    $contents = trim($min).";";
                } catch (Exception $e) {
                    //throw $e; 
                }
            } 
        }
        return trim($contents)."\n";
    }

    /**
     * checks for minifiable content
     *
     * add comment //minify in first line to force minfy
     * add comment //!minify in first line to exclude
     *
     * @todo check for already compressed files
     *
     * @param string $file
     * @param string $contents
     * @return string
     */
    protected function _isMinifyable($file, $contents)
    {
        $file = strtolower($file);
        $pi = pathinfo($file);
        if ($pi['extension']!=='js') {
            return false;
        }
        $firstLines = substr($contents, 0, 1000);
        if (strpos($firstLines,'//minify') === 0) {
            return true;
        }
        if (strpos($firstLines,'//!minify') === 0) {
            return false;
        }
        if (strpos($file,'.min.js')) {
            return false;
        }
        $lines = explode("\n", $firstLines, 4);
        if (count($lines)<2) {
            return false;
        }
        return true;
    }


}
