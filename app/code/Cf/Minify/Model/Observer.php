<?php
/**
 * Codefathers Minify for JS and CSS
 *
 * @category    Cf
 * @package     Cf_Minify
 * @copyright   Copyright (c) codefathers 2017
 */


/**
 * Class Cf_Minify_Model_Observer
 */
class Cf_Minify_Model_Observer
{

    /**
     *
     * @param Varien_Event_Observer $observer
     * @return null 
     * 
     */
    public function handleDesignPackageBeforeMergeJsFile(Varien_Event_Observer $observer)
    {
        $doCompress = Mage::getStoreConfigFlag('dev/js/compress');
        if (!$doCompress) {
            return ;
        }
        $file = $observer->getFile();
        $result = $observer->getResult();
        $content = $result->content;
        $doMinify = $this->_isMinifyable($file, $content);
        if ($doMinify) {
            try {
                $js = Cf_Minify_Model_Compress_Abstract::factory('js');
                $content = $js->minify($content);
                $content = trim($content).";\n";
                $result->content = $content;
            } catch (Exception $e) {
                //throw $e; 
            }
        }   
    }
    
    
    /**
     * checks for minifiable content
     *
     * add comment //minify in first line to force minfy
     * add comment //!minify in first line to exclude
     *
     * @todo check for already compressed files
     *
     * @param string $file
     * @param string $contents
     * @return boolean
     */
    protected function _isMinifyable($file, $contents)
    {
        $file = strtolower($file);
        $pi = pathinfo($file);
        if ($pi['extension']!=='js') {
            return false;
        }
        if (strpos($file,'.min.js')) {
            return false;
        }
        $firstLines = trim(substr($contents, 0, 50));
        if (strpos($firstLines,'//minify') === 0) {
            return true;
        }
        if (strpos($firstLines,'//!minify') === 0) {
            return false;
        }
        $lines = explode("\n", $firstLines, 4);
        if (count($lines)<2) {
            return false;
        }
        return true;
    }


    
}
