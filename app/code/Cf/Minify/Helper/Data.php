<?php
/**
 * Codefathers Minify for JS and CSS
 *
 * @category    Cf
 * @package     Cf_Minify
 * @copyright   Copyright (c) codefathers 2017
 */


/**
 * Class Cf_Minify_Helper_Data
 */
class Cf_Minify_Helper_Data extends Mage_Core_Helper_Abstract
{


    /**
     * logs messages
     *
     * @param mixed $msg
     * @param integer $level
     */
    public function log($msg, $level = Zend_Log::INFO)
    {
        /* log exceptions to central exeption log */
        if ($msg instanceof Exception) {
            $level = Zend_Log::ERR;
            Mage::logException($msg);
            Mage::log($msg->getMessage(), $level, 'cf-minify.log');
            return;
        }
        Mage::log($msg, $level, 'cf-minify.log');
    }

   
}
